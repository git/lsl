Lazy Shopping List protocol
===========================

GET /
-----

Returns a JSON object with the following keys:

 - `lists`: URI for the list of shopping lists available. Referred to as
   `/list` below.

GET /list
----------

Returns a JSON object with the following keys:

 - `lists`: a list of objects with the following keys:
   - `uri`: the list URI. Referred to as `/list/$list_id` below. The part after
     the last `/` (`$list_id`) is an integer that is unique among all shopping
     lists
   - `name`: list name as entered by users
 - `lists_version`: a number that is incremented by 1 each time the list of
   shopping lists changes. Changes include adding, removing and renaming a
   shopping list, but not changes to individual shopping list items.

GET /list/$list_id
------------------

Returns a JSON object with the following keys:

 - `items`: a list of shopping list item objects with keys:
   - `uri`: the URI of the individual list item (refered to as
     `/list/$list_id/$item_id` below. The part after the last `/` (`$list_id`)
     is a positive integer that is unique amont the items in the shopping list.
   - `description`: item description
   - `done`: a boolean flag marking the item as done
 - `version`: a number that is incremented by 1 each time the list items
   change. Changes include adding, removing, renaming and marking a shopping
   list item as done or pending.
 - `lists_version`: the current version of the list of shopping lists

GET /list/$list_id/$item_id
---------------------------

Returns a JSON object with the following keys:

 - `description`: the description of the shopping list item
 - `done`: a boolean flag marking the item as done
 - `list_version`: the current version of the list
 - `lists_version`: the current version of the list of shopping lists

POST /list
----------

Creates a new list. Request body is a JSON object with the following keys:

 - `name`: list name as entered by the user

Response is a JSON object with the following keys:

 - `uri`: the URI of the list (`/list/$list_id`)
 - `version`: the initial version of the list
 - `lists_version`: the version of the list of lists

PUT /list/$list_id
------------------

Modifies a list. Request body is a JSON object with the following keys:

 - `name`: new list name
 - `version`: current list version as known by the client

Successful (HTTP status 200) response is a JSON object with the following keys:

 - `version`: the current version of the list
 - `lists_version`: the new version of the list of lists

Possible error responses:

 - HTTP status 409 (conflict): the list has been modified
   Suggested client action is to fetch a fresh copy of the list data, present
   the differences to the user and either cancel the request and keep the newly
   fetched data as current, or re-submit the change using the fresh version
   number.

DELETE /list/$list_id
---------------------

Deletes the list. No request body. Always succeeds, even if the list doesn't
exist server-side, in which case the `lists_version` isn't changed.

Returns JSON object with the following keys:

 - `lists_version`: the new version of the list of lists.

POST /list/$list_id
-------------------

Adds an item to the shopping list. Request body is a JSON object with the
following keys:

 - `description`: the description of the new shopping list item
 - `done`: (boolean) the done state

Successful response is a JSON object with the following keys:

 - `uri`: the URI of the newly added shopping list item
 - `version`: version of the item
 - `list_version`: version of the containing list
 - `lists_version`: version of the list of lists

PUT /list/$list_id/$item_id
---------------------------

Modifies a shopping list item. Request body is a JSON object with the following
keys:

 - `description`: new item description
 - `done`: (boolean) desired item state
 - `version`: (required) the version of the item as known by the client

Successful response is a JSON object with the following keys:

 - `version`: the new version of the item
 - `list_version`: the current version of the list
 - `lists_version`: the current version of the list of shopping lists

Possible error responses:

 - HTTP status 409 (conflict): the item has been modified
   Suggested client action is to fetch a fresh copy of the item data, present
   the differences to the user and either cancel the request and keep the newly
   fetched data as current, or re-submit the change using the fresh version
   number.

DELETE /list/$list_id/$item_id
------------------------------

Deletes a list item. No request body. Always succeeds, even if the list item
doesn't exist server-side, in which case the `list_version` isn't changed.

Returns JSON object with the following keys:

 - `list_version`: the new version of the shopping list whose item was deleted.
 - `lists_version`: the current version of the list of shopping lists
