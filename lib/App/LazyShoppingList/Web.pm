use v5.26;
use warnings;
use utf8;

package App::LazyShoppingList::Web;
use Dancer2;

our $VERSION = '0.1';

use JSON();

use experimental 'signatures';

set charset => 'UTF-8';

# main web-application
get '/' => sub {
    set environment => $ENV{PLACK_ENV};
    template 'index' => { 'title' => 'App::REST::LazyShoppingList' };
};

true;
