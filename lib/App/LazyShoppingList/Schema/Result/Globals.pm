use v5.28;
use warnings;
use utf8;

package App::LazyShoppingList::Schema::Result::Globals;

use parent qw( DBIx::Class::Core );

__PACKAGE__->table('globals');

__PACKAGE__->add_columns(
    lists_version => { data_type => 'integer' },
    db_revision   => { data_type => 'integer' },
);


1;
