use v5.28;
use warnings;
use utf8;

package App::LazyShoppingList::Schema::Result::ShoppingListItem;

use parent qw( DBIx::Class::Core );

__PACKAGE__->table('shopping_list_items');

__PACKAGE__->add_columns(
    id => {
        data_type => 'integer',
        is_auto_increment => 1,
    },
    shopping_list => {
        data_type => 'integer',
    },
    description => {
        data_type => 'text',
    },
    done => {
        data_type => 'boolean',
    },
    version  => {
        data_type => 'integer',
    },
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to( shopping_list =>
        'App::LazyShoppingList::Schema::Result::ShoppingList' =>
        'shopping_list' );

1;
