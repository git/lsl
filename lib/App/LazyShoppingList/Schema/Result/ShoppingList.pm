use v5.28;
use warnings;
use utf8;

package App::LazyShoppingList::Schema::Result::ShoppingList;

use parent qw( DBIx::Class::Core );

__PACKAGE__->table('shopping_lists');

__PACKAGE__->add_columns(
    id => { data_type => 'integer',
        is_auto_increment => 1,
    },
    name => { data_type => 'text' },
    version => { data_type => 'integer' },
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->has_many(
    items => 'App::LazyShoppingList::Schema::Result::ShoppingListItem' =>
        'shopping_list' );

1;
