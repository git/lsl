use v5.28;
use warnings;
use utf8;

package App::LazyShoppingList::Schema;

use base qw(DBIx::Class::Schema);

__PACKAGE__->load_namespaces;

1;
