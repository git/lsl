#!/usr/bin/perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib";


use App::LazyShoppingList::API::v1;
use App::LazyShoppingList::Web;

use Plack::Builder;

builder {
    enable 'Deflater';
    mount '/api/v1' => App::LazyShoppingList::API::v1->to_app;
    mount '/'       => App::LazyShoppingList::Web->to_app;
}


