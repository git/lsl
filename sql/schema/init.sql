create table globals(
    lists_version bigint not null,
    db_revision bigint not null );

insert into globals(lists_version, db_revision) values(1, 0);

create table shopping_lists(
    id bigserial primary key,
    name text not null,
    version bigint not null default 1 );

create table shopping_list_items(
    id bigserial primary key,
    shopping_list bigint not null
        references shopping_lists(id)
        on delete cascade,
    description text not null,
    version bigint not null default 1,
    done boolean not null default false );
